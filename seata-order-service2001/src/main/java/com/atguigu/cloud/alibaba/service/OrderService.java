package com.atguigu.cloud.alibaba.service;

import com.atguigu.cloud.alibaba.domain.Order;

public interface OrderService
{
    /**
     * 创建订单
     */
    void create(Order order);
}
