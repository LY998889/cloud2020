package com.atguigu.cloud.alibaba.controller;

import com.atguigu.cloud.alibaba.domain.CommonResult;
import com.atguigu.cloud.alibaba.domain.Order;
import com.atguigu.cloud.alibaba.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class OrderController
{
    @Resource
    private OrderService orderService;

    /**
     * 创建订单
     * @param order 订单实体类
     * @return 统一返回
     */
    @GetMapping("/order/create")
    public CommonResult create(Order order)
    {
        orderService.create(order);
        return new CommonResult(200,"订单创建成功");
    }
}
