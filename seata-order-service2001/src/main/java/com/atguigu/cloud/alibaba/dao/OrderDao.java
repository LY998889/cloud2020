package com.atguigu.cloud.alibaba.dao;

import com.atguigu.cloud.alibaba.domain.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface OrderDao
{
    /**
     * 创建订单
     * @param order 订单实体类
     *
     */
    void create(Order order);

    /**
     * 修改订单状态为已完成
     * @param userId 用户ID
     * @param status 订单状态
     */
    void update(@Param("userId") Long userId,@Param("status") Integer status);
}
