package com.atguigu.cloud.alibaba.controller;

import com.atguigu.cloud.alibaba.domain.CommonResult;
import com.atguigu.cloud.alibaba.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StorageController
{
    @Autowired
    private StorageService storageService;

    /**
     * 扣减库存
     * @param productId 产品ID
     * @param count 库存数量
     * @return 统一返回类
     */
    @RequestMapping("/storage/decrease")
    public CommonResult decrease(Long productId, Integer count){

        storageService.decrease(productId, count);

        return new CommonResult(200,"扣减库存成功");
    }
}
