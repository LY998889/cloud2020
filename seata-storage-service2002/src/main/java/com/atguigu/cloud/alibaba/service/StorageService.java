package com.atguigu.cloud.alibaba.service;

import org.apache.ibatis.annotations.Param;

public interface StorageService
{
    /**
     * 扣减库存
     * @param productId
     * @param count
     */
    void decrease(Long productId,Integer count);
}
