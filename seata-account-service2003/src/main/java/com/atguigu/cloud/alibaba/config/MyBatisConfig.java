package com.atguigu.cloud.alibaba.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@MapperScan({"com.atguigu.cloud.alibaba.dao"})
public class MyBatisConfig {

}
 
 

